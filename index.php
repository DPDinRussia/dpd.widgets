<?php
require_once __DIR__ .'/vendor/dpd.sdk/src/autoload.php';

$params = require __DIR__ .'/config.php';
$config = new \Ipol\DPD\Config\Config($params);

$action = isset($_GET['action']) ? $_GET['action'] : '';
$action = trim($action, '/');

try {
    switch ($action) {
        case 'check-status':
            if (empty($_REQUEST['number'])) {
                throw new Exception('Track number is empty');
            }

            $params['KLIENT_NUMBER'] = '3355779903';
            $params['KLIENT_KEY']    = '7BC65C96D4DE98CD17072C88BFE21CC703A38607';
            $config = new \Ipol\DPD\Config\Config($params);
            $client = \Ipol\DPD\API\User\User::getInstanceByConfig($config);

            $ret = [];
            $service = $client->getService('tracking-order');
            $result = $service->getByOrderNumber($_REQUEST['number'], $_REQUEST['phone']);

            if ($result) {
                foreach ($result['events'] as $item) {
                    if (!$item['pointCity']) {
                      continue;
                    }

                    $ret[] = [
                        'name'     => $item['eventName'] . ( $item['isReturn'] ? ' (возврат)' : '' ),
                        'reason'   => $item['eventReason'],
                        'date'     => $item['eventMoment'],
                        'code'     => $item['pointCity'],
                    ];
                }

                usort($ret, function($a, $b) {
                    $date1 = str_replace('.', '-', $a['date']);
                    $date2 = str_replace('.', '-', $b['date']);

                    return strtotime($date2) - strtotime($date1);
                });
            }
        break;

        case 'autocomplete':
            $ret = [];

            if (!empty($_REQUEST['term'])) {
                $table  = \Ipol\DPD\DB\Connection::getInstance($config)->getTable('location');
                $PDO    = $table->getPDO();

                $query = $PDO->prepare('SELECT ORIG_NAME FROM b_ipol_dpd_location WHERE CITY_NAME LIKE :like_string ORDER BY IS_CITY DESC, ORIG_NAME limit 10');

                $query->execute([
                    'like_string' => mb_strtolower($_REQUEST['term']) .'%'
                ]);

                while($item = $query->fetch()) {
                    $ret[] = $item['ORIG_NAME'];
                }
            }

            print json_encode($ret);
            die;
        break;

        case 'calculate':
            $table  = \Ipol\DPD\DB\Connection::getInstance($config)->getTable('location');

            $locationFrom = $table->findFirst([
                'where' => 'ORIG_NAME = :orig_name',
                'bind' => ['orig_name' => $_REQUEST['locationFrom']]
            ]);

            $locationTo   = $table->findFirst([
                'where' => 'ORIG_NAME = :orig_name',
                'bind' => ['orig_name' => $_REQUEST['locationTo']]
            ]);

            if (!$locationFrom) {
                throw new Exception('Location from not found');
            }

            if (!$locationTo) {
                throw new Exception('location to not found');
            }

            $shipment = new \Ipol\DPD\Shipment($config);
            $shipment->setSender($locationFrom);
            $shipment->setReceiver($locationTo);
            $shipment->setSelfPickup(!!$_REQUEST['selfPickup']);
            $shipment->setSelfDelivery(!!$_REQUEST['selfDelivery']);

            $price = floatval(str_replace(',', '.', $_REQUEST['price']));

            if ($price) {
                $shipment->setPrice($price);
                $shipment->setDeclaredValue(true);
            } else {
                $shipment->setDeclaredValue(false);
            }

            if (!empty($_REQUEST['dimensions']) && is_array($_REQUEST['dimensions'])) {
                $items = [];

                foreach ($_REQUEST['dimensions'] as $dimension) {
                    $items[] = [
                        'WEIGHT' => $dimension['weight'],
                        'DIMENSIONS' => [
                            'WIDTH'  => $dimension['width'],
                            'HEIGHT' => $dimension['height'],
                            'LENGTH' => $dimension['length'],
                        ],
                        'QUANTITY'   => $dimension['count'],
                    ];
                }

                $shipment->setItems($items, $shipment->getPrice());

            } else {
                $shipment->setDimensions(
                    str_replace(',', '.', $_REQUEST['volume']) * 1000000,
                    1,
                    1,
                    str_replace(',', '.', $_REQUEST['weight'])
                );
            }

            $ret = array_values(
                $shipment->calculator()->calculateAll(false, sizeof($shipment->getItems()) > 0)
            );

            usort($ret, function($a, $b) {
              return $a['COST'] - $b['COST'];
            });
        break;

        default:
            throw new Exception('Unknown action');
        break;
    }

    $ret = [
        'status' => 'ok',
        'data'   => $ret,
    ];
} catch (Exception $e) {
    $ret = [
        'status' => 'err',
        'data'   => $e->getMessage()
    ];
}

print json_encode($ret);
