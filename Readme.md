# Основные возможности

## Калькулятор стоимости доставки

* Расчет доставки для указанных габаритов
* Возможность гибкой настройки расчета стоимости доставки
* Гибкая настройка отображения и простое подключение виджета

## Проверка статуса заказа

* Получение и вывод историю изменения статусов заказа по его номеру
* Гибкая настройка отображения и простое подключение виджета

# Установка и настройка виджетов

1. Скачайте [архив](https://bitbucket.org/DPDinRussia/dpd.widgets/get/master.zip) и загрузите его содержимое на Ваш сайт в отдельную директорию, например `dpd`.

2. Отредактируйте файл `config.php` заполнив поля
* `KLIENT_NUMBER` - клиентский номер (номер договора с DPD)
* `KLIENT_KEY`  - авторизационный токен
Описание всех доступных параметров для настройки Вы можете найти [здесь](https://bitbucket.org/DPDinRussia/dpd.sdk/src/master/)

Предупреждение: Для безопасности, файл `config.php` рекомендуется хранить в недоступном для внешнего доступа месте.

3. На нужную страницу добавить код подключения стилей и скриптов виджета (рекомендуется его расположить внутри тега <head>):

```html
<link rel="stylesheet" type="text/css" media="screen" href="/dpd/css/style.css" />
<script src="/dpd/js/jquery.dpd.js"></script>
```

Для корректной работы виджета на сайте должена быть подключена библиотека `jquery` до подключения скриптов виджетов
```html
<script src="/dpd/js/jquery.min.js"></script>
```

В виджете калькулятора возможен вывод подсказок автодополнения при вводе названий городов отправления и назначения.
Для работы подсказок Вы должны подключить библиотеку `jquery-ui`
```html
<link rel="stylesheet" type="text/css" media="screen" href="/dpd/css/jquery-ui.min.css" />
<script src="/dpd/js/jquery-ui.min.js"></script>
```

4. В месте где будет выводится виджет добавьте контейнеры
```html
<div id="calculator"><!-- здесь будет выводится виджет калькулятора --></div>
<div id="status"><!-- здесь будет выводится виджет проверки статосов --></div>
```

5. Инициализируйте виджет
Для инициализации виджета добавьте следующий код на страницу

```html
<script>
    // инициализация калькулятора
    $('#calculator').dpdCalc({
        apiUrl: '/dpd/index.php',
    });

    // инициализация статуса
    $('#status').dpdStatus({
        apiUrl: '/dpd/index.php',
    });
</script>
```

Если все сделано правильно, то при перезагрузке страницы в указанных контейнерах должны появится формы виджетов.

## Пример подключения виджета

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>DPD Widgets</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/dpd/css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/dpd/css/style.css" />
    <script src="/dpd/js/jquery.min.js"></script>
    <script src="/dpd/js/jquery-ui.min.js"></script>
    <script src="/dpd/js/jquery.inputmask.bundle.min.js"></script>
    <script src="/dpd/js/jquery.dpd.js"></script>
    <script>
        $(function() {
            $('#status').dpdStatus({
                apiUrl: '/dpd/index.php',
            });

            $('#calculator').dpdCalc({
                apiUrl: '/dpd/index.php',
            });
        })
    </script>
</head>
<body>
    <div style="width: 500px; margin: 0 auto;">
        <div id="status"></div>
        <div id="calculator"></div>
    </div>
</body>
</html>
```

# Настройки
Каждый из виджетов имеет набор параметров с помощью которох можно настроить его работу.
Эти параметры указываются при инициализации виджета.

## Виджет калькулятора
* `apiUrl` - путь к распакованому архиву на сайте
* `templates.form` - в этом параметре указывается html разметка виджета. Если передать пустое значение, предполагается что разметка виджета уже выведена на страницу.
* `templates.result` - в этом параметре указывается html разметка одной строки результата. В этой строке возможно использование placeholder'ов которые будут заменены на конкретные значения.
    * `#NAME#` - имя тарифа
    * `#PRICE#` - стоимость доставки
    * `#DAYS#` - сроки доставки
* `templates.resultEmpty` - в этом параметре указывается html разметка которая будет выведена при пустом результате.
* `inputs` - этот параметр содержит селекторы для поиска элементов необходимых для работы виджета
* `inputs.from` - селектор поля ввода названия города отправления, по умолчания `.dpd-location-from`
* `inputs.to` - селектор поля ввода названия города назначения, по умолчания `.dpd-location-to`
* `inputs.weight` - селектор поля ввода веса посылки, по умолчания `.dpd-weight`
* `inputs.volume` - селектор поля ввода объема посылки, по умолчания `.dpd-volume`
* `inputs.price` - селектор поля ввода объявленной ценности посылки, по умолчания `.dpd-price`
* `inputs.button` - селектор кнопки начала расчета, по умолчания `.dpd-submit`
* `inputs.result` - селектор контейнера результата, по умолчания `.dpd-result`
* `inputs.resultWrap` - селектор контейнера результата, по умолчания `.dpd-result-panel`
* `inputs.loader` - селектор загрузчика, по умолчания `.dpd-loader`
* `values` - в этом параметре можно задать значения полей по умолчанию
* `values.from` - указывается название пункта отправления
* `values.to` - указывается название пункта назначения
* `values.selfPickup` - 0 - доставка от склада, 1 - доставка от двери
* `values.selfDelivery` - 0 - доставка до склада, 1 - доставка до двери
* `values.weight` - указывается вес по умолчанию (кг)
* `values.volume` - указывается объем по умолчанию (м3)
* `values.price` - указывается ОЦ по умолчанию
* `values.dimensions` - указываются габариты по умолчанию, массив с объектами описывающими габариты посылок. Параметр не совместим с `values.volume`
```js
[
  {width: 1, height: 2, length: 3, weight: 1, count: 1},
  {width: 1, height: 2, length: 3, weight: 1, count: 1},
  {width: 1, height: 2, length: 3, weight: 1, count: 1},
]
```
* `display` - данный параметр служит для настройки отображаемых полей формы
* `display.from` - Отображать поле "Пункт отправления" (true/false)
* `display.to` - Отображать поле "Пункт отправления" (true/false)
* `display.selfPickup` - Отображать поле "Доставка от" (true/false)
* `display.selfDelivery` - Отображать поле "Доставка до" (true/false)
* `display.weight` - Отображать поле "Вес" (true/false)
* `display.volume` - Отображать поле "Объем" (true/false)
* `display.price` - Отображать поле "ОЦ" (true/false)
* `display.dimensions` - Отображать поле для расширенного ввода габаритов (true/false)

### События
В процессе расчета стоимости доставки модуль инициализирует 2 вида событий, подписавшишь на которые можно модифицировать
данные для расчета или результат.

* `OnBeforeCalculate.dpd` - событие вызывается перед передачей данных API. Позволяет модифицировать передаваемые данные,
например задать габариты по умолчанию.

```js
$('#calculator').on('OnBeforeCalculate.dpd', function(e, data) {
    data.dimensions = [
      {width: 1, height: 2, length: 3, weight: 1, count: 1},
      {width: 1, height: 2, length: 3, weight: 1, count: 1},
      {width: 1, height: 2, length: 3, weight: 1, count: 1},
    ];
});
```

* `OnAfterCalculate.dpd` - событие вызывается после получения результата расчета от сервера. Позволяет модифицировать
список тарифов, например можно изменить сроки доставки.

```js
$('#calculator').on('OnBeforeCalculate.dpd', function(e, result) {
    if (result.status == 'ok') {
        for (var i in result.data) {
            result.data[i].DAYS = result.data[i].DAYS + 3;
        }
    }
});
```

### Поддерживаемые тарифы DPD
Виджет расчета стоимости доставки будет использовать следующие тарифы в своей работе
* DPD 18:00
* DPD ECONOMY
* DPD CLASSIC domestic
* DPD EXPRESS
* DPD Online Express
* DPD OPTIMUM
* DPD MAX domestic
* DPD Online Max

## Виджет проверки статуса заказа
* `apiUrl` - путь к распакованому архиву на сайте
* `templates.form` - в этом параметре указывается html разметка виджета. Если передать пустое значение, предполагается что разметка виджета уже выведена на страницу.
* `templates.result` - в этом параметре указывается html разметка одной строки результата. В этой строке возможно использование placeholder'ов которые будут заменены на конкретные значения.
    * # DATE # - дата изменения заказа
    * # CODE # - код статуса
    * # NAME # - название статуса
    * # REASON # - описание статуса
* `templates.resultEmpty` - в этом параметре указывается html разметка которая будет выведена при пустом результате.
* `inputs` - этот параметр содержит селекторы для поиска элементов необходимых для работы виджета
* `inputs.number` - селектор поля ввода номера заказа, по умолчания `.dpd-number`
* `inputs.button` - селектор кнопки начала расчета, по умолчания `.dpd-submit`
* `inputs.result` - селектор контейнера результата, по умолчания `.dpd-result`
* `inputs.resultWrap` - селектор контейнера результата, по умолчания `.dpd-result-panel`
* `inputs.loader` - селектор загрузчика, по умолчания `.dpd-loader`
