(function (window, document, $) {
    var defaults = {
        calculator: {
            templates: {
                form: '<div class="dpd-wrap">'
                    + ' <div class="dpd-panel">'
                    + '     <div class="dpd-panel__heading">Калькулятор по России и странам Таможенного союза</div>'
                    + '     <div class="dpd-panel__body dpd-form">'
                    + '         <div class="dpd-form__group dpd-group-from">'
                    + '             <label class="dpd-form__label">Пункт отправления <b>*</b></label>'
                    + '             <input class="dpd-form__control dpd-location-from" type="text">'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-selfPickup">'
                    + '             <label class="dpd-form__label">&nbsp;</label>'
                    + '             <div>'
                    + '                 <label>'
                    + '                     <input class="dpd-self-pickup" name="dpd-self-pickup" value="0" type="radio" checked="checked">'
                    + '                     от двери'
                    + '                 </label>'
                    + '                 <label>'
                    + '                     <input class="dpd-self-pickup" name="dpd-self-pickup" value="1" type="radio">'
                    + '                     от пункта выдачи'
                    + '                 </label>'
                    + '             </div>'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-to">'
                    + '             <label class="dpd-form__label">Пункт назначения <b>*</b></label>'
                    + '             <input class="dpd-form__control dpd-location-to" type="text">'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-selfDelivery">'
                    + '             <label class="dpd-form__label">&nbsp;</label>'
                    + '             <div>'
                    + '                 <label>'
                    + '                     <input class="dpd-self-delivery" name="dpd-self-delivery" value="0" type="radio" checked="checked">'
                    + '                     до двери'
                    + '                 </label>'
                    + '                 <label>'
                    + '                     <input class="dpd-self-delivery" name="dpd-self-delivery" value="1" type="radio">'
                    + '                     до пункта выдачи'
                    + '                 </label>'
                    + '             </div>'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-weight">'
                    + '             <label class="dpd-form__label">Вес отправки, кг <b>*</b></label>'
                    + '             <input class="dpd-form__control dpd-weight" type="text">'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-volume">'
                    + '             <label class="dpd-form__label">Объем отправки, м3</label>'
                    + '             <input class="dpd-form__control dpd-volume" type="text">'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-form-dropdown dpd-group-dimensions">'
                    + '           <a href="" class="dpd-form-dropdown__title">Задать габариты для точного расчета</a>'
                    + '           <div class="dpd-form-dropdown__content">'
                    + '             <div class="dpd-form__group">'
                    + '               <input type="submit" class="dpd-button dpd-dimensions-add" value="Добавить посылку">'
                    + '             </div>'
                    + '             <div class="dpd-dimensions-group">'
                    + '                 <div class="dpd-form__group dpd-form__group--dimensions dpd-dimensions">'
                    + '                     <div class="dpd-form__group">'
                    + '                         <span class="dpd-dimensions-remove">Удалить</span>'
                    + '                     </div>'
                    + '                     <div class="dpd-form__group">'
                    + '                         <label class="dpd-form__label">Вес, кг</label>'
                    + '                         <input class="dpd-form__control dpd-dimensions-weight" type="text">'
                    + '                     </div>'
                    + '                     <div class="dpd-form__group">'
                    + '                         <label class="dpd-form__label">Длина, см</label>'
                    + '                         <input class="dpd-form__control dpd-dimensions-length" type="text">'
                    + '                     </div>'
                    + '                     <div class="dpd-form__group">'
                    + '                         <label class="dpd-form__label">Ширина, см</label>'
                    + '                         <input class="dpd-form__control dpd-dimensions-width" type="text">'
                    + '                     </div>'
                    + '                     <div class="dpd-form__group">'
                    + '                         <label class="dpd-form__label">Высота, см</label>'
                    + '                         <input class="dpd-form__control dpd-dimensions-height" type="text">'
                    + '                     </div>'
                    + '                     <div class="dpd-form__group">'
                    + '                         <label class="dpd-form__label">Кол-во</label>'
                    + '                         <input class="dpd-form__control dpd-dimensions-count" type="text">'
                    + '                     </div>'
                    + '                 </div>'
                    + '             </div>'
                    + '           </div>'
                    + '         </div>'
                    + '         <div class="dpd-form__group dpd-group-price">'
                    + '             <label class="dpd-form__label">Объявленная ценность, руб</label>'
                    + '             <input class="dpd-form__control dpd-price" type="text">'
                    + '         </div>'
                    + '         <div class="dpd-form__group">'
                    + '             <input type="submit" class="dpd-button dpd-submit" value="Рассчитать">'
                    + '         </div>'
                    + '     </div>'
                    + ' </div>'
                    + ' <div class="dpd-panel dpd-result-panel">'
                    + '     <div class="dpd-panel__heading">Рекомендуемые услуги</div>'
                    + '     <div class="dpd-panel__body">'
                    + '         <table class="dpd-table">'
                    + '             <thead>'
                    + '                 <tr>'
                    + '                     <td>Название услуги</td>'
                    + '                     <td>Стоимость доставки, руб</td>'
                    + '                     <td>Срок доставки, раб. дни</td>'
                    + '                 </tr>'
                    + '             </thead>'
                    + '             <tbody class="dpd-result">'
                    + '                 <tr>'
                    + '                     <td colspan="3" align="center">Для расчета стоимости заполните форму выше</td>'
                    + '                 </tr>'
                    + '             </tbody>'
                    + '         </table>'
                    + '     </div>'
                    + ' </div>'
                    + ' <div class="dpd-loader">'
                    + '     <div class="dpd-loader__mask dpd-mask"></div>'
                    + '     <div class="dpd-loader__spiner">Пожалуйста<br>подождите</div>'
                    + ' </div>'
                    + '</div>'
                ,
                result: '<tr><td>#NAME#</td><td>#PRICE#</td><td>#DAYS#</td></tr>',
                resultEmpty: '<tr><td colspan="3">Не удалось расчитать стоимость доставки</td></tr>',
            },

            inputs: {
                from: '.dpd-location-from',
                to: '.dpd-location-to',
                volume: '.dpd-volume',
                weight: '.dpd-weight',
                price: '.dpd-price',
                button: '.dpd-submit',
                result: '.dpd-result',
                resultWrap: '.dpd-result-panel',
                resultToggle: '.dpd-result-toggle',
                loader: '.dpd-loader',
                pickup: '.dpd-self-pickup:checked',
                pickupAll: '.dpd-self-pickup',
                delivery: '.dpd-self-delivery:checked',
                deliveryAll: '.dpd-self-delivery',
                dimensions: '.dpd-dimensions',
                dimensionsAdd: '.dpd-dimensions-add',
                dimensionsRemove: '.dpd-dimensions-remove',
                dimensionsGroup: '.dpd-dimensions-group',
                dimensionsWidth: '.dpd-dimensions-width',
                dimensionsHeight: '.dpd-dimensions-height',
                dimensionsLength: '.dpd-dimensions-length',
                dimensionsWeight: '.dpd-dimensions-weight',
                dimensionsCount: '.dpd-dimensions-count',
            },

            display: {
              from: true,
              to: true,
              pickup: true,
              delivery: true,
              weight: true,
              volume: true,
              dimensions: true,
              price: true,
            },

            values: {
                from: '',
                to: '',
                selfPickup: '',
                selfDelivery: '',
                weight: '',
                volume: '',
                price: '',
                dimensions: []
            },

            auth: {
                client: '',
                token: '',
            },

            apiUrl: '',
        },

        status: {
            templates: {
                form: '<div class="dpd-wrap dpd-wrap-short">'
                    + ' <div class="dpd-panel">'
                    + '   <div class="dpd-panel__heading">Проверка статуса заказа</div>'
                    + '   <div class="dpd-panel__body">'
                    + '     <div class="dpd-form__group">'
                    + '         <label class="dpd-form__label">Номер заказа <b>*</b></label>'
                    + '         <input class="dpd-form__control dpd-number" type="text">'
                    + '     </div>'
                    + '     <div class="dpd-form__group">'
                    + '         <label class="dpd-form__label">Номер телефона <b>*</b></label>'
                    + '         <input class="dpd-form__control dpd-phone" type="text">'
                    + '     </div>'
                    + '     <div class="dpd-form__group">'
                    + '         <input type="submit" class="dpd-button dpd-submit" value="Проверить">'
                    + '     </div>'
                    + '    </div>'
                    + ' </div>'
                    + ' <div class="dpd-panel dpd-result-panel">'
                    + '    <div class="dpd-panel__heading">Статус заказа</div>'
                    + '    <div class="dpd-panel__body">'
                    + '        <table class="dpd-table">'
                    + '            <thead>'
                    + '                <tr>'
                    + '                    <td width="150px">Дата</td>'
                    + '                    <td width="150px">Город</td>'
                    + '                    <td>Описание</td>'
                    + '                </tr>'
                    + '            </thead>'
                    + '            <tbody class="dpd-result">'
                    + '                <tr>'
                    + '                    <td colspan="3" align="center">Для проверки статуса заказа заполните форму выше</td>'
                    + '                </tr>'
                    + '            </tbody>'
                    + '            <tfoot>'
                    + '               <td colspan="3" align="center">'
                    + '                 <a href="" class="dpd-result-toggle">История состояний</a>'
                    + '               </td>'
                    + '            </tfoot>'
                    + '        </table>'
                    + '    </div>'
                    + ' </div>'
                    + ' <div class="dpd-loader">'
                    + '     <div class="dpd-loader__mask dpd-mask"></div>'
                    + '     <div class="dpd-loader__spiner">Пожалуйста<br>подождите</div>'
                    + ' </div>'
                    + '</div>'
                ,
                result: '<tr><td>#DATE#</td><td>#CODE#</td><td>#NAME#</td></tr>',
                resultEmpty: '<td colspan="3" align="center">По указанному трек-номеру не удалось получить информацию</td>'
            },

            inputs: {
                number: '.dpd-number',
                phone: '.dpd-phone',
                button: '.dpd-submit',
                result: '.dpd-result',
                resultWrap: '.dpd-result-panel',
                resultToggle: '.dpd-result-toggle',
                loader: '.dpd-loader',
            },

            auth: {
                client: '',
                token: '',
            },

            apiUrl: '',
        },

        error: {
            templates: ''
                + '<div class="dpd-wrap dpd-wrap--absolute">'
                + '     <div class="dpd-mask"></div>'
                + '     <div class="dpd-error-box">'
                + '         <div class="dpd-panel">'
                + '             <div class="dpd-panel__heading">Ошибка!</div>'
                + '             <div class="dpd-panel__body">'
                + '                 <div class="dpd-form__group dpd-text">'
                + '                     #ERROR#'
                + '                 </div>'
                + '                 <div class="dpd-form__group">'
                + '                     <input type="submit" class="dpd-button dpd-submit" value="Закрыть">'
                + '                 </div>'
                + '             </div>'
                + '         </div>'
                + '     </div>'
                + '</div>'
            ,

            inputs: {
                text: '.dpd-text',
                button: '.dpd-submit',
            }
        }
    };

    /**************************************************************************/

    var SDK = function (auth, apiUrl) {
        this.auth = auth;
        this.apiUrl = apiUrl || '';
    }

    $.extend(SDK.prototype, {
        calculate: function (parms, callback) {
            return this.send(
                'calculate',
                parms,
                callback
            );
        },

        checkStatus: function (trackNumber, phoneNumber, callback) {
            return this.send(
                'check-status',
                { number: trackNumber, phone: phoneNumber },
                callback
            );
        },

        send: function (action, parms, callback) {
            this.ajax && this.ajax.abort();
            this.ajax = $.ajax({
                type: 'POST',
                url: this.apiUrl + '?action='+ action,
                data: $.extend(true, parms, { auth: this.auth }),
                dataType: 'json',
                success: function (data) {
                    callback && callback(data);
                },
                failure: function () {
                    callback && callback(false);
                }
            })
        }
    });

    /**************************************************************************/

    var Plugin = function (owner, parms) {
        this.owner = owner;

        this.parms = $.extend(true, defaults[this.type], parms);
        this.sdk   = new SDK(this.parms.auth, this.parms.apiUrl);
    }

    $.extend(Plugin.prototype, {
        init: function () {
            if (this.parms.templates.form) {
                this.owner.innerHTML = this.parms.templates.form;
            }

            $(this.parms.inputs.button, this.owner).click($.proxy(this.submit, this));

            for (var i in this.parms.inputs) {
                $(this.owner).on('focus', this.parms.inputs[i], function() {
                    $(this).removeClass('error');
                });
            }

            $(this.parms.inputs.resultToggle).click($.proxy(this.resultToggle, this));
        },

        resultToggle: function() {
          $('> div', this.owner).toggleClass('dpd-wrap-short');

          return false;
        },

        submit: function () {
            var data = this.getData();

            if (!data) {
                return false;
            }

            var loader     = $(this.parms.inputs.loader, this.owner).show();
            var resultWrap = $(this.parms.inputs.resultWrap, this.owner).hide();
            var submit     = $(this.parms.inputs.button, this.owner).attr('disabled', 'disabled');

            var callback = $.proxy(function (data) {
                loader.hide();

                if (data.status != 'ok') {
                    this.showError(data.data);
                } else {
                    this.render(data.data);
                    resultWrap.show();
                }
                
                submit.removeAttr('disabled');
            }, this);

            this.getResult(data, callback);

            return false;
        },

        showError: function(error) {
            if (!this.errorBox) {
                var html = defaults.error.templates;

                this.errorBox = $(html).hide().appendTo('body');
                $(defaults.error.inputs.button, this.errorBox).click($.proxy(this.hideError, this))
            }

            $(defaults.error.inputs.text, this.errorBox).html(error)
            this.errorBox.show();
        },

        hideError: function() {
            this.errorBox && this.errorBox.hide();
        }
    });

    var PluginCalc = function () {
        Plugin.apply(this, arguments);
    };

    $.extend(PluginCalc.prototype, Plugin.prototype, {
        type: 'calculator',

        init: function () {
            Plugin.prototype.init.apply(this, arguments);

            if ($.fn.autocomplete) {
                $(this.parms.inputs.from + ',' + this.parms.inputs.to, this.owner).autocomplete({
                    source: this.parms.apiUrl + '?action=autocomplete',
                    minLength: 3,
                });
            }

            if ($.fn.inputmask) {
                $(this.parms.inputs.volume +','+ this.parms.inputs.weight +','+ this.parms.inputs.price, this.owner).inputmask('numeric', {
                    radixPoint: ',',
                    rightAlign: false,
                });
            }

            $(this.parms.inputs.from, this.owner).val(this.parms.values.from || '');
            $(this.parms.inputs.to, this.owner).val(this.parms.values.to || '');
            $(this.parms.inputs.pickupAll, this.owner).filter('[value="'+ this.parms.values.selfPickup +'"]').prop('checked', true);
            $(this.parms.inputs.deliveryAll, this.owner).filter('[value="'+ this.parms.values.selfDelivery +'"]').prop('checked', true);
            $(this.parms.inputs.weight, this.owner).val(this.parms.values.weight || '');
            $(this.parms.inputs.volume, this.owner).val(this.parms.values.volume || '');
            $(this.parms.inputs.price, this.owner).val(this.parms.values.price   || '');
            
            this.setDimensions(this.parms.values.dimensions || []);
            this.parms.templates.dimensions = {
                class : $(this.parms.inputs.dimensions).attr('class'),
                html  : $(this.parms.inputs.dimensions, this.owner).html(),
            }

            var self = this;

            $(this.owner).on('click', '.dpd-form-dropdown__title', function() {
                $dropdown = $(this).closest('.dpd-form-dropdown');

                if ($dropdown.hasClass('dpd-form-dropdown--show')) {
                    $dropdown.removeClass('dpd-form-dropdown--show');

                    $(self.parms.inputs.weight, self.owner).prop('disabled', false).val('');
                    $(self.parms.inputs.volume, self.owner).prop('disabled', false).val('');
                } else {
                    $dropdown.addClass('dpd-form-dropdown--show');

                    $(self.parms.inputs.weight, self.owner).prop('disabled', true).val('');
                    $(self.parms.inputs.volume, self.owner).prop('disabled', true).val('');
                }

                return false;
            });
            
            $(this.owner).on('click', this.parms.inputs.dimensionsAdd, function() {
              self.addDimensions();
              return false;
            });

            $(this.owner).on('click', this.parms.inputs.dimensionsRemove, function() {
                $(this).closest(self.parms.inputs.dimensions).remove();
                return false;
            });

            for (var i in this.parms.display) {
              if (this.parms.display[i] !== false) {
                $('.dpd-group-'+ i, this.owner).show();
              } else {
                $('.dpd-group-'+ i, this.owner).hide();
              }
            }
        },

        getData: function () {
            var self = this;
            var error = false;
            var data = {
                locationFrom : $(this.parms.inputs.from, this.owner).val(),
                locationTo   : $(this.parms.inputs.to, this.owner).val(),
                selfPickup   : $(this.parms.inputs.pickup, this.owner).val(),
                selfDelivery : $(this.parms.inputs.delivery, this.owner).val(),
                price        : $(this.parms.inputs.price, this.owner).val(),
                weight       : $(this.parms.inputs.weight, this.owner).val(),
                volume       : $(this.parms.inputs.volume, this.owner).val(),
                dimensions   : $(this.parms.inputs.weight, this.owner).prop('disabled') 
                    ? [].reduce.call($(this.parms.inputs.dimensions, this.owner), function(ret, group) {
                          var item = {
                            weight: $(self.parms.inputs.dimensionsWeight, group).val() || 0,
                            width:  $(self.parms.inputs.dimensionsWidth, group).val()  || 0,
                            height: $(self.parms.inputs.dimensionsHeight, group).val() || 0,
                            length: $(self.parms.inputs.dimensionsLength, group).val() || 0,
                            count:  $(self.parms.inputs.dimensionsCount, group).val()  || 0,
                          }

                          if (item.weight > 0 && item.width > 0 && item.height > 0 && item.length > 0 && item.count > 0) {
                            ret.push(item);
                          } else {
                            error = true;

                            if (item.weight <= 0) {
                                $(self.parms.inputs.dimensionsWeight, group).addClass('error');
                            }

                            if (item.width <= 0) {
                                $(self.parms.inputs.dimensionsWidth, group).addClass('error');
                            }

                            if (item.height <= 0) {
                                $(self.parms.inputs.dimensionsHeight, group).addClass('error');
                            }

                            if (item.length <= 0) {
                                $(self.parms.inputs.dimensionsLength, group).addClass('error');
                            }

                            if (item.count <= 0) {
                                $(self.parms.inputs.dimensionsCount, group).addClass('error');
                            }
                          }

                          return ret;
                        }, [])

                    : []
                ,
            }

            if (!error) {
                $(this.owner).trigger('OnBeforeCalculate.dpd', [data]);

                data.weight = data.dimensions.reduce(function(ret, item) {
                  return ret + (item.weight * item.count);
                }, 0) || parseFloat(data.weight.replace(',', '.'));

                data.volume = data.dimensions.reduce(function(ret, item) {
                  return ret + (((item.width * item.height * item.length) / 1000000) * item.count);
                }, 0) || parseFloat(data.volume.replace(',', '.'));

                $(this.parms.inputs.weight, this.owner).val(data.weight.toFixed(6));
                $(this.parms.inputs.volume, this.owner).val(data.volume.toFixed(6));

                if (!data.weight) {
                    $(this.parms.inputs.weight, this.owner).addClass('error');
                    error = true;
                }
            }

            if (!data.locationFrom) {
                $(this.parms.inputs.from, this.owner).addClass('error');
                error = true;
            }

            if (!data.locationTo) {
                $(this.parms.inputs.to, this.owner).addClass('error');
                error = true;
            }

            if (error) {
                this.showError('Не все обязательные поля были заполнены!');
                return false;
            }

            return data;
        },

        getResult: function (parms, callback) {
            var self = this;

            return this.sdk.calculate(parms, function(data) {
                $(self.owner).trigger('onAfterCalculate.dpd', [data]);
                callback(data);
            });
        },

        render: function (data) {
            var html = '';

            if (data.length > 0) {
                for (var i in data) {
                    html += this.parms.templates.result
                        .replace('#NAME#', data[i].SERVICE_NAME)
                        .replace('#PRICE#', data[i].COST)
                        .replace('#DAYS#', data[i].DAYS)
                }

            } else {
                html = this.parms.templates.resultEmpty;
            }

            $(this.parms.inputs.result, this.owner).html(html);

            if (data.length > 0) {
              $('> div', this.owner).addClass('dpd-wrap--result');
            } else {
              $('> div', this.owner).removeClass('dpd-wrap--result');
            }
        },

        addDimensions: function(dimensions) {
          var group = $('<div />')
                .addClass(this.parms.templates.dimensions.class)
                .html(this.parms.templates.dimensions.html)
          ;

          // $(this.parms.inputs.dimensions, this.owner).first();

          $(this.parms.inputs.dimensionsGroup).append(group);

          group
            // .clone(true, true)
            // .insertBefore(group)
            .find('input').val('')
          ;

          if (dimensions != window.undefined) {
            $(this.parms.inputs.dimensionsWeight, group).val(dimensions.weight);
            $(this.parms.inputs.dimensionsWidth, group).val(dimensions.width);
            $(this.parms.inputs.dimensionsHeight, group).val(dimensions.height);
            $(this.parms.inputs.dimensionsLength, group).val(dimensions.length);
            $(this.parms.inputs.dimensionsCount, group).val(dimensions.count);
          }
        },

        setDimensions: function(dimensions) {
          if (dimensions.length > 0) {
            for (var i in dimensions) {
              // console.log(dimensions)
              this.addDimensions(dimensions[i]);
            }

            $('.dpd-form-dropdown', this.owner).addClass('dpd-form-dropdown--show');
            $(this.parms.inputs.weight).val('').attr('disabled', 'disabled');
            $(this.parms.inputs.volume).val('').attr('disabled', 'disabled');
          }
        },
    });

    var PluginStatus = function () {
        Plugin.apply(this, arguments);
    };

    $.extend(PluginStatus.prototype, Plugin.prototype, {
        type: 'status',

        init: function () {
            Plugin.prototype.init.apply(this, arguments);

            if ($.fn.inputmask) {
                // $(this.parms.inputs.number, this.owner).inputmask({
                //     mask: '***********'
                // })

                $(this.parms.inputs.phone, this.owner).inputmask({
                    mask: '7 (999) 999-99-99',
                    autoUnmask: true
                });
            }
        },

        getData: function () {
            var data = {
                number: $(this.parms.inputs.number, this.owner).val(),
                phone : $(this.parms.inputs.phone, this.owner).val(),
            }

            if (!data.number) {
                this.showError('Укажите номер заказа!');
                return false;
            }

            if (!data.phone) {
                this.showError('Укажите номер телефона');
                return false;
            }

            if (data.phone.substr(0, 1) != '7') { 
                data.phone = '7'+ data.phone;
            }

            return data;
        },

        getResult: function (parms, callback) {
            return this.sdk.checkStatus(parms.number, parms.phone, callback)
        },

        render: function (data) {
            var html = '';

            if (data.length > 0) {
                for (var i in data) {
                    html += this.parms.templates.result
                        .replace('#DATE#', data[i].date)
                        .replace('#CODE#', data[i].code)
                        .replace('#NAME#', data[i].name)
                        .replace('#REASON#', data[i].reason ? ('(' + data[i].reason + ')') : '')
                }

            } else {
                html = this.parms.templates.resultEmpty;
            }

            $(this.parms.inputs.result, this.owner).html(html);

            if (data.length > 0) {
              $('> div', this.owner).addClass('dpd-wrap--result');
            } else {
              $('> div', this.owner).removeClass('dpd-wrap--result');
            }
        }
    });


    $.fn.dpdCalc = function (parms) {
        return this.each(function () {
            var plugin = new PluginCalc(this, parms);
            plugin.init();
        })
    }

    $.fn.dpdStatus = function (parms) {
        return this.each(function () {
            var plugin = new PluginStatus(this, parms);
            plugin.init();
        });
    }

})(window, document, jQuery);
