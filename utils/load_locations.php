<?php
require_once __DIR__ .'/../vendor/dpd.sdk/src/autoload.php';

$parms  = require __DIR__ .'/../config.php';
$config = new \Ipol\DPD\Config\Config($parms);

$api    = \Ipol\DPD\API\User\User::getInstanceByConfig($config);
$table  = \Ipol\DPD\DB\Connection::getInstance($config)->getTable('location');

$loader = new \Ipol\DPD\DB\Location\Agent($api, $table);
$loader->loadAll();
$loader->loadCashPay();
